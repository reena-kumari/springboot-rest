package com.example.project1;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.project1.exception.UserNotFoundException;
import com.example.project1.service.User;
import com.example.project1.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	@GetMapping("/users")
	public User getUsers() {
		return userService.getUser();
	}
	
	@GetMapping("/users/{id}")
	public EntityModel<User> getUserByid(@PathVariable int id) {
		if(id>5) {
			 //throw new ResponseStatusException(HttpStatus.NOT_FOUND,"id: "+id);
			throw new UserNotFoundException("id="+id);
		}
		User user = userService.getUser();
		//"all-users", SERVER_PATH + "/users"
				//retrieveAllUsers
		
		EntityModel<User> resource = EntityModel.of(user);
				
		WebMvcLinkBuilder allUsers = 
						linkTo(methodOn(this.getClass()).getUsers());
				
				resource.add(allUsers.withRel("all-users"));
				
		WebMvcLinkBuilder self = 
						linkTo(methodOn(this.getClass()).getUserByid(id));
				
				resource.add(self.withRel("self"));
				
				//HATEOAS
		
		return resource;
	}
	
	@PostMapping("/users")
	public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
		System.out.println(user);
		User savedUser = userService.save(user);
		
		//returning status and correct uri for created resource
		 URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
		.buildAndExpand(savedUser.getId()).toUri();
		
		return ResponseEntity.created(location).body(savedUser);
		//return savedUser;
	}

}
