package com.example.project1.service;

import com.fasterxml.jackson.annotation.JsonFilter;

@JsonFilter("demo-filter")
public class DemoBean {
	
	private static int count=0;
	private int id;
	private String name;
	private String desc;
	public DemoBean(String string, String string2) {
		id = ++count;
		name = string;
		desc = string2;
	}
	public String getName() {
		return name;
	}
	public String getDesc() {
		return desc;
	}
	public int getId() {
		return id;
	}

}
