package com.example.project1.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value= {"field1"})
public class SomeBean {
	
	private String field1;
	@JsonIgnore
	private String field2;
	public SomeBean(String string, String string2) {
		field1 = string;
		field2 = string2;
	}
	public String getField1() {
		return field1;
	}
	public String getField2() {
		return field2;
	}

}
