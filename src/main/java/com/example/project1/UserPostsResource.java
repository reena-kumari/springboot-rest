package com.example.project1;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.project1.exception.UserNotFoundException;
import com.example.project1.service.Post;
import com.example.project1.service.PostRepository;
import com.example.project1.service.User;
import com.example.project1.service.UserRepository;

@RestController
public class UserPostsResource {
	
	@Autowired
	private UserRepository repo;
	
	@Autowired
	private PostRepository postRepo;

	@GetMapping("/v2/users")
	public List<User> getUsers(){
		return repo.findAll();
	}
	
	@GetMapping("/v2/users/{id}")
	public User getUser(@PathVariable int id){
		Optional<User> user = repo.findById(id);
		if(user.isEmpty()) {
			throw new UserNotFoundException("User id :"+id);
		}
		return user.get();
	}
	
	@PostMapping("v2/users")
	public ResponseEntity<User> save(@RequestBody User user) {
		repo.save(user);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(user.getId()).toUri();
					
		return ResponseEntity.created(location).body(user);
	}
	
	@PostMapping("v2/users/{id}/posts")
	public ResponseEntity<Post> save(@PathVariable int id, @RequestBody Post post) {
		
		Optional<User> user = repo.findById(id);
		if(user.isEmpty()) {
			throw new UserNotFoundException("User id not found :"+id);
		}
		post.setUser(user.get());
		postRepo.save(post);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(post.getId()).toUri();
					
		return ResponseEntity.created(location).body(post);
	}
	
	
	@DeleteMapping("/v2/users/{id}")
	public void deleteUser(@PathVariable int id) {
		 repo.deleteById(id);
	}
	
	@GetMapping("/v2/users/{id}/posts")
	public List<Post> getPostById(@PathVariable int id){
		Optional<User> user = repo.findById(id);
		if(user.isEmpty()) {
			throw new UserNotFoundException("User id not found :"+id);
		}
		return user.get().getPosts();
	}
	
	
	
}
