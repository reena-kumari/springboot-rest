package com.example.project1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//Configuration
//enable swagger
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	public static final Contact DEFAULT_CONTACT = new Contact(
    "reena",
    "",
    "reena@test.com");
	
	public static final ApiInfo DEFAULT = new ApiInfo(
    "SpringBoot demo",
    "Spring boot basic app",
    "1.0",
    "urn:tos",
    DEFAULT_CONTACT,
    "Apache 2.0",
    "http://www.apache.org/licenses/LICENSE-2.0",
    new ArrayList<>());
	
	@Bean
	public Docket api() {
		//basic swagger with defult values
		//return new Docket(DocumentationType.SWAGGER_2);
		
		Set<String> produces = new HashSet<>(Arrays.asList("application/xml","application/json"));
		return new Docket(DocumentationType.SWAGGER_2) //defualt config
				.apiInfo(DEFAULT)  //change info part of response
				.produces(produces) //override default value with what application can produce, it is applicable to all api
				.consumes(produces); //default value will be picked from each individual api
	}

}
