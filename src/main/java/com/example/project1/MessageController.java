package com.example.project1;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
	
	@Autowired
	private MessageSource messageSource;
	
	//Way-1 - getting locale value from request header
	@GetMapping(path = "/message-internationalized")
	public String helloWorldInternationalized(@RequestHeader(value="Accept-Language", required=false) Locale locale) {
		return messageSource.getMessage("good.morning.message", null,locale);
	}
	
	//way-2 getting request value from localcontext
	@GetMapping(path = "/message-internationalized-2")
	public String helloInternationalized() {
		System.out.println(messageSource.getMessage("good.morning.message", null, Locale.US));
		System.out.println(messageSource.getMessage("good.morning.message", null, Locale.FRANCE));
		return messageSource.getMessage("good.morning.message", null,
				LocaleContextHolder.getLocale());
	}

}
