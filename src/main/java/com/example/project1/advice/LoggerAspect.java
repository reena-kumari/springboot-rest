package com.example.project1.advice;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.example.project1.service.User;

@Aspect
@Component
public class LoggerAspect {
	
	@Before("execuation(public * com.example.project1.UserPostsResource.getUsers())")
	public void log() {
		System.out.println("get advice called");
	}
}
