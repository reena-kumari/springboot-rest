package com.example.project1;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.project1.service.DemoBean;
import com.example.project1.service.SomeBean;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RestController
public class FilterController {

	@GetMapping("/static-filter")
	public List<SomeBean> some() {
		return Arrays.asList(
				new SomeBean("value1","value2"),
				new SomeBean("value1","value2"));
	}
	
	@GetMapping("/dynamic-filter")
	public MappingJacksonValue dynamicFilter() {
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("name","desc"); //will return all filed listed here
		FilterProvider filters = new SimpleFilterProvider().addFilter("demo-filter", filter);
		MappingJacksonValue mapping = new MappingJacksonValue(new DemoBean("reena","dev"));
		mapping.setFilters(filters);
		return mapping;
		
	}
	
	@GetMapping("/dynamic-filter-list")
	public MappingJacksonValue dynamicFilter1() {
		DemoBean a = new DemoBean("reena","dev");
		DemoBean bb = new DemoBean("jadu","dev");
		
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("name","id"); //will return all filed listed here

		FilterProvider filters = new SimpleFilterProvider().addFilter("demo-filter", filter);

		MappingJacksonValue mapping = new MappingJacksonValue(Arrays.asList(a, bb));

		mapping.setFilters(filters);
		
		return mapping;
	}
	
}
